import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../Services/data.service'

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  step = 0;
  showAddItem = false;
  cartArray:any;
  constructor(private DataService: DataService, private router: Router, private route: ActivatedRoute) {
  
  }

  ngOnInit(): void {
    this.getCartList();
  }


  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  addQuantity() {
    this.cartArray.quantity = this.cartArray.quantity + 1;
  }

  minusQuantity() {
    if (this.cartArray.quantity > 1) {
      this.cartArray.quantity = this.cartArray.quantity - 1;
    }
  }
  showItem() {
    this.showAddItem = true;
  }

  async getCartList() {
    this.DataService.getCartList().subscribe(async res => {     
    let req = await res;
    this.cartArray = req["response"];
  })
}
}
