import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DataService } from '../../Services/data.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  pinCode: number;
  productDetailResponse: any;
  productId = 0;
  resp: any;
  brandName:string;
  brandId: number;
  description: string;
  list_category_id: number;
  main_category_id: number;
  productTypeId: number;
  product_master_id: number
  product_varient: any;
  available_qty: string;
  cartQuantity: string;
  discount: string;
  discountPrice: string;
  in_qty: string;
  out_qty: string;
  variantId: number
  variantImage: string;
  variantImage2: string;
  variantImage3: string;
  variantMrp: number
  varientValue: string;
  tax_id: number

  constructor(private DataService: DataService,
    private route: ActivatedRoute, private Router: Router) {
    this.route.params.subscribe(params => {
      this.productId = params.id
      console.log(params)
    });
  }
  ngOnInit(): void {
    this.getProductDetail()
  }

  getProductDetail() {
    var req = {
      productId: this.productId,
      platform: "web",
      userId: null
    }
    this.DataService.getProductDetail(req).subscribe(async res => {
      this.resp = res
      this.resp.map(response => {
        this.brandName= response.brandName;
        this.brandId = response.brandId;
        this.description = response.description;
        this.list_category_id = response.list_category_id;
        this.main_category_id = response.main_category_id;
        this.productTypeId = response.productTypeId;
        this.product_master_id = response.product_master_id;
        this.product_varient = response.product_varient;  
      });

      this.product_varient.map(productVarient => {  
      this.available_qty = productVarient.available_qty;
      this.cartQuantity = productVarient.cartQuantity;
      this.discount = productVarient.discount;
      this.discountPrice = productVarient.discountPrice;
      this.in_qty = productVarient.in_qty;
      this.out_qty = productVarient.out_qty;
      this.variantId = productVarient.variantId;
      this.variantImage = productVarient.variantImage;
      this.variantImage2 = productVarient.variantImage2;
      this.variantMrp = productVarient.variantMrp;
      this.varientValue = productVarient.varientValue;     
      });     

    })
  }


  async addToCart() {
    var req = {
      userId: null,
      productVariantId: this.variantId,
      quantity: this.cartQuantity,
      platform: "web",     
      session_name:"test"
    }   
    this.DataService.addToCart(req).subscribe(async res => {
      this.productDetailResponse = await res
      if (this.productDetailResponse.message == "Success") {
        console.log("Products are added to Cart successfully")
        this.Router.navigate(['/add-to-cart/']);
      }         
    })
  }

  async checkForPincodeAvailablity() {  
    this.DataService.checkForPincodeAvailablity(this.pinCode).subscribe(async res => {
      this.productDetailResponse = await res    
        console.log(this.productDetailResponse['response'])
    })
  }  
}
