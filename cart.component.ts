import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../Services/data.service'
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartArray:any;
  showAddItem = false;
  constructor(private DataService: DataService, private router: Router, private route: ActivatedRoute,) {
  
  }

  ngOnInit(): void {
    this.getCartList();
  }
  addQuantity() {
    this.cartArray.quantity =   this.cartArray.quantity + 1;
  }

  minusQuantity() {
    if (  this.cartArray.quantity > 1) {
      this.cartArray.quantity =  this.cartArray.quantity - 1;
    }
  }
  showItem() {
    this.showAddItem = true;
  }

  async continueShopping() {
    this.router.navigate(['']);
  }  

  async checkOut() {
    this.router.navigate(['/checkout/']);
  }  
  async getCartList() {
      this.DataService.getCartList().subscribe(async res => {     
      let req = await res;
      this.cartArray = req["response"];
    })
  }
  

}
