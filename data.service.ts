// saranya
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "http://annapurnadev-env.eba-ainusmma.us-east-1.elasticbeanstalk.com";

  constructor(public http: HttpClient,
  ) { }
  getCategoryData() {
    return this.http.get('/category/getMainCategory', {}).pipe(map(res => res));
  };
  getSubCategoryData(id) {
    return this.http.get(`/category/getSubCategoryWithMainCategory/${id}`, {}).pipe(map(res => res));
  };
  getlistCategoryData(id) {
    return this.http.get(`/category/getListCategoryWithSubcategory/${id}`, {}).pipe(map(res => res));
  };
  getProductList(req) {
    var login = localStorage.getItem('loginData');
    req.userId = login

    return this.http.post(`/products/filterProduct`, req).pipe(map(res => res));
  };
  getProductDetail(req) {
    var login = localStorage.getItem('loginData');
    req.userId = parseInt(login)
    return this.http.post(`/products/viewProduct`, req).pipe(map(res => res));
  };
  // getAllSubCategory() {
  //   return this.http.get(`/category/getSubCategory`, {}).pipe(map(res => res));
  // };
  // getAllListCategory() {
  //   return this.http.get(`/category/getListCategory`, {}).pipe(map(res => res));
  // };
  getSideMenuCategoryData(mainCategoryId, subCategoryId) {
    return this.http.get(`/products/productSideMenu/${mainCategoryId}/${subCategoryId}`, {}).pipe(map(res => res));
  };

  getCartList() {
    var userId = localStorage.getItem('loginData');
    return this.http.get(`/cart/myCart/${userId}`, {}).pipe(map(res => res));
  };

  addToCart(req) {
    var login = localStorage.getItem('loginData');
    req.userId = parseInt(login)
    return this.http.post(`/cart/addToCart`, req).pipe(map(res => res));
  }

  addUserAddress(req) {
    var login = localStorage.getItem('loginData');
    req.userId = parseInt(login)
    return this.http.post(`/user/addUserAddress`, req).pipe(map(res => res));
  }
  
  checkForPincodeAvailablity(pincode) {
    return this.http.get(`/pincode/checkPincodeForAvailability/${pincode}`, {}).pipe(map(res => res));
  }
}